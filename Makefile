include .env

migrate:
	python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

dev:
	python src/manage.py runserver localhost:8000

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

dc_lint:
	docker-compose run app pipenv run isort --check --diff .
	docker-compose run app pipenv run flake8 --config setup.cfg
	docker-compose run app pipenv run black --check --config pyproject.toml .

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

start_bot:
	python src/manage.py start_telegram_bot

build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down

test:
	pytest -v

dc_test:
	docker run --rm ${IMAGE_APP}
	docker-compose run --rm app bash -c "cd src && pipenv run pytest -v"
