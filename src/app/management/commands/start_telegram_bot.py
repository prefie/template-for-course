from django.core.management.base import BaseCommand

from app.internal.bot.bot import TelegramBot
from config.settings import TELEGRAM_TOKEN


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        bot = TelegramBot(TELEGRAM_TOKEN)
        bot.start()
