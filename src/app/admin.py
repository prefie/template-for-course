from django.contrib import admin

from app.internal.admins.presentation.admin import AdminUserAdmin
from app.internal.auth.presentation.admin import IssuedToken
from app.internal.bank_accounts.presentation.admin import BankAccount
from app.internal.cards.presentation.admin import Card
from app.internal.transactions.presentation.admin import Transaction
from app.internal.users.presentation.admin import User

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
