from django.contrib import admin

from app.internal.bank_accounts.db.models import BankAccount


@admin.register(BankAccount)
class BankAccount(admin.ModelAdmin):
    pass
