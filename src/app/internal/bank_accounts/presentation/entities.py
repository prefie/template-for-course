from ninja.orm import create_schema

from ..db.models import BankAccount

BankAccountSchema = create_schema(BankAccount, exclude=["owner"])


class BankAccountOut(BankAccountSchema):
    pass
