from ninja import Path

from app.internal.bank_accounts.presentation.entities import BankAccountOut
from app.internal.exceptions.exceptions import ForbiddenException, NotFoundException


class BankAccountHandlers:
    def __init__(self, bank_account_repository):
        self._bank_account_repo = bank_account_repository

    def get_bank_account(self, request, bank_account_number: str = Path(...)):
        bank_account = self._bank_account_repo.get_bank_account(bank_account_number)
        if not bank_account:
            raise NotFoundException(bank_account_number, "bank_account")

        if bank_account.owner.telegram_id != request.user_id:
            raise ForbiddenException()

        return BankAccountOut.from_orm(bank_account)
