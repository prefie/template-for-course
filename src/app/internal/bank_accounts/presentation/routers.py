from ninja import Router

from app.internal.bank_accounts.db.repositories import BankAccountRepository
from app.internal.bank_accounts.presentation.entities import BankAccountOut
from app.internal.bank_accounts.presentation.handlers import BankAccountHandlers
from app.internal.responses.responses import ErrorResponse


def add_bank_accounts_api(api):
    bank_account_repo = BankAccountRepository()
    bank_account_handlers = BankAccountHandlers(bank_account_repo)
    add_bank_accounts_router(api, bank_account_handlers)


def add_bank_accounts_router(api, bank_account_handlers):
    bank_account_router = get_bank_accounts_router(bank_account_handlers)
    api.add_router("/bank_accounts", bank_account_router)


def get_bank_accounts_router(bank_accounts_handlers):
    router = Router(tags=["bank_accounts"])

    router.add_api_operation(
        "/{int:bank_account_number}",
        ["GET"],
        bank_accounts_handlers.get_bank_account,
        response={200: BankAccountOut, 400: ErrorResponse, 403: ErrorResponse},
    )

    return router
