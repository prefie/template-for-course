import random
import string
from uuid import uuid4

from django.db import models

from app.internal.users.db.models import User


def generate_bank_account_number():
    letters = string.digits
    return "".join(random.choice(letters) for _ in range(20))


class BankAccount(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    number = models.CharField(max_length=20, unique=True, default=generate_bank_account_number)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    objects = models.Manager()
