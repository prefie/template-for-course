from django.db import transaction
from django.db.models import Q

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.transactions.db.models import Transaction


class BankAccountRepository:
    def get_bank_account_numbers(self, user_id):
        return BankAccount.objects.filter(owner_id=user_id).values_list("number", flat=True)

    def get_bank_account(self, bank_account_number):
        return BankAccount.objects.filter(number=bank_account_number).first()

    def transfer_money_by_bank_account(self, bank_account, bank_account_to_transfer, amount):
        bank_account = self.get_bank_account(bank_account)
        bank_account_to_transfer = self.get_bank_account(bank_account_to_transfer)
        if not bank_account or not bank_account_to_transfer or bank_account.amount < amount or amount <= 0:
            return False
        with transaction.atomic():
            bank_account.amount -= amount
            bank_account_to_transfer.amount += amount
            bank_account.save()
            bank_account_to_transfer.save()
            Transaction.objects.create(
                bank_account_from=bank_account, bank_account_to=bank_account_to_transfer, amount=amount
            )
            return True

    def get_bank_account_transactions(self, bank_account_number):
        return (
            Transaction.objects.filter(
                Q(bank_account_from__number=bank_account_number) | Q(bank_account_to__number=bank_account_number)
            )
            .order_by("-date")
            .values(
                "bank_account_from__owner__nickname",
                "bank_account_from__number",
                "bank_account_to__owner__nickname",
                "bank_account_to__number",
                "amount",
                "date",
            )
        )
