from datetime import datetime

import jwt
from django.conf import settings

from app.internal.auth.db.models import IssuedToken


class IssuedTokenRepository:
    def generate_access_token(self, telegram_id, delta):
        return jwt.encode(
            {"telegram_id": telegram_id, "expires": datetime.now().timestamp() + delta},
            settings.JWT_SECRET,
            algorithm="HS256",
        )

    def generate_refresh_token(self, delta):
        return jwt.encode({"expires": datetime.now().timestamp() + delta}, settings.JWT_SECRET, algorithm="HS256")

    def generate_access_and_refresh_tokens(self, user, access_token_delta, refresh_token_delta):
        access_token = self.generate_access_token(user.telegram_id, access_token_delta)
        refresh_token = self.generate_refresh_token(refresh_token_delta)

        IssuedToken.objects.create(jti=refresh_token, user=user)

        return access_token, refresh_token

    def get_token(self, refresh_token):
        return IssuedToken.objects.filter(jti=refresh_token).first()

    def revoke_token(self, refresh_token):
        token = self.get_token(refresh_token)
        token.revoked = True
        token.save()

    def revoke_all_tokens(self, user):
        user.refresh_tokens.update(revoked=True)
        user.save()

    def has_token_expired(self, refresh_token):
        try:
            payload = jwt.decode(refresh_token, settings.JWT_SECRET, algorithms=["HS256"])
        except jwt.exceptions.InvalidSignatureError:
            return True

        time = payload.get("expires")

        if time is None:
            return True

        return time < datetime.now().timestamp()
