from ninja import Schema


class LoginSchema(Schema):
    nickname: str
    password: str


class JWTSchema(Schema):
    access_token: str
    refresh_token: str
