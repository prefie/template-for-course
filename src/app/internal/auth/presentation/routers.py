from ninja import Body, Router

from app.internal.auth.db.repositories import IssuedTokenRepository
from app.internal.auth.presentation.entities import JWTSchema, LoginSchema
from app.internal.auth.presentation.handlers import AuthHandlers
from app.internal.responses.responses import ErrorResponse
from app.internal.users.db.repositories import UserRepository


def add_auth_api(api):
    issued_token_repo = IssuedTokenRepository()
    user_repo = UserRepository()
    auth_handler = AuthHandlers(issued_token_repo, user_repo)
    add_auth_router(api, auth_handler)


def add_auth_router(api, auth_handlers):
    auth_router = get_auth_router(auth_handlers)
    api.add_router("/auth", auth_router)


def get_auth_router(auth_handlers):
    router = Router(tags=["auth"])

    router.add_api_operation(
        "/login",
        ["POST"],
        auth_handlers.login,
        auth=None,
        response={200: JWTSchema, 401: ErrorResponse},
    )

    router.add_api_operation(
        "/refresh",
        ["POST"],
        auth_handlers.refresh,
        auth=None,
        response={200: JWTSchema, 400: ErrorResponse},
    )

    return router
