from ninja import Body

from app.internal.auth.presentation.entities import JWTSchema, LoginSchema
from app.internal.exceptions.exceptions import BadRequestException, UnauthorizedException


class AuthHandlers:
    def __init__(self, issued_token_repository, user_repository):
        self._issued_token_repo = issued_token_repository
        self._user_repository = user_repository

    def login(self, request, login: LoginSchema = Body(...)):
        user = self._user_repository.get_user_by_nickname(login.nickname)
        hashed_password = self._user_repository.hash_password(login.password)
        if not user or not user.password or user.password != hashed_password:
            raise UnauthorizedException()

        access_token, refresh_token = self._issued_token_repo.generate_access_and_refresh_tokens(
            user, 2 * 60 * 60, 24 * 60 * 60
        )
        return JWTSchema(access_token=access_token, refresh_token=refresh_token)

    def refresh(self, request, refresh_token):
        token = self._issued_token_repo.get_token(refresh_token)
        if not token:
            raise BadRequestException("invalid token")

        if token.revoked:
            self._issued_token_repo.revoke_all_tokens(token.user)
            raise BadRequestException("refresh token was revoked")

        self._issued_token_repo.revoke_token(token.jti)
        if self._issued_token_repo.has_token_expired(token.jti):
            raise BadRequestException("refresh token has expired")

        access_token, refresh_token = self._issued_token_repo.generate_access_and_refresh_tokens(
            token.user, 2 * 60 * 60, 24 * 60 * 60
        )
        return JWTSchema(access_token=access_token, refresh_token=refresh_token)
