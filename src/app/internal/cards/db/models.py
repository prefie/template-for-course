import random
import string
from uuid import uuid4

from django.db import models

from app.internal.bank_accounts.db.models import BankAccount


def generate_card_number():
    letters = string.digits
    return "".join(random.choice(letters) for _ in range(16))


class Card(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    number = models.CharField(max_length=16, unique=True, default=generate_card_number)
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)

    objects = models.Manager()
