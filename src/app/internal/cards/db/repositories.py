from django.db import transaction
from django.db.models import Q

from app.internal.cards.db.models import Card
from app.internal.transactions.db.models import Transaction


class CardRepository:
    def get_cards(self, user_id):
        return Card.objects.filter(bank_account__owner=user_id).values_list("number", flat=True)

    def get_card(self, card_number):
        return Card.objects.filter(number=card_number).first()

    def transfer_money_by_card(self, card, card_to_transfer, amount):
        card = self.get_card(card)
        card_to_transfer = self.get_card(card_to_transfer)
        if not card or not card_to_transfer or card.bank_account.amount < amount or amount <= 0:
            return False

        with transaction.atomic():
            card.bank_account.amount -= amount
            card_to_transfer.bank_account.amount += amount
            card.bank_account.save()
            card_to_transfer.bank_account.save()
            card.save()
            card_to_transfer.save()
            Transaction.objects.create(
                bank_account_from=card.bank_account,
                bank_account_to=card_to_transfer.bank_account,
                card_from=card,
                card_to=card_to_transfer,
                amount=amount,
            )
            return True

    def get_card_transactions(self, card_number):
        return (
            Transaction.objects.filter(Q(card_from__number=card_number) | Q(card_to__number=card_number))
            .order_by("-date")
            .values(
                "card_from__bank_account__owner__nickname",
                "card_to__bank_account__owner__nickname",
                "card_from__number",
                "card_to__number",
                "amount",
                "date",
            )
        )
