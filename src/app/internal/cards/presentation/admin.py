from django.contrib import admin

from app.internal.cards.db.models import Card


@admin.register(Card)
class Card(admin.ModelAdmin):
    pass
