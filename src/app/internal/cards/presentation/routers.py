from ninja import Router

from app.internal.cards.db.repositories import CardRepository
from app.internal.cards.presentation.handlers import CardHandlers


def add_cards_api(api):
    card_repo = CardRepository()
    card_handlers = CardHandlers(card_repo)
    add_cards_router(api, card_handlers)


def add_cards_router(api, card_handlers):
    cards_router = get_cards_router(card_handlers)
    api.add_router("/cards", cards_router)


def get_cards_router(cards_handlers):
    router = Router(tags=["cards"])

    return router
