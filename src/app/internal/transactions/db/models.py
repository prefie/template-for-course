from uuid import uuid4

from django.db import models

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.cards.db.models import Card


class Transaction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    date = models.DateTimeField(auto_now_add=True)
    bank_account_from = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name="transactions_from")
    card_from = models.ForeignKey(Card, on_delete=models.SET_NULL, null=True, related_name="transactions_from")
    bank_account_to = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name="transactions_to")
    card_to = models.ForeignKey(Card, on_delete=models.SET_NULL, null=True, related_name="transactions_to")
    amount = models.DecimalField(max_digits=15, decimal_places=2)

    objects = models.Manager()
