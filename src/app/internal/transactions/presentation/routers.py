from ninja import Router

from app.internal.transactions.db.repositories import TransactionRepository
from app.internal.transactions.presentation.handlers import TransactionHandlers


def add_transactions_api(api):
    transaction_repo = TransactionRepository()
    transaction_handlers = TransactionHandlers(transaction_repo)
    add_transactions_router(api, transaction_handlers)


def add_transactions_router(api, transaction_handlers):
    transactions_router = get_transactions_router(transaction_handlers)
    api.add_router("/transactions", transactions_router)


def get_transactions_router(transaction_handlers):
    router = Router(tags=["transactions"])

    # TODO: add URI

    return router
