from django.contrib import admin

from app.internal.transactions.db.models import Transaction


@admin.register(Transaction)
class Transaction(admin.ModelAdmin):
    pass
