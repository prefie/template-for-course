from ninja import NinjaAPI

from app.internal.auth.presentation.jwt_auth import HTTPJWTAuth
from app.internal.auth.presentation.routers import add_auth_api
from app.internal.bank_accounts.presentation.routers import add_bank_accounts_api
from app.internal.cards.presentation.routers import add_cards_api
from app.internal.exceptions.handlers import add_exception_handlers
from app.internal.transactions.presentation.routers import add_transactions_api
from app.internal.users.presentation.routers import add_users_api


def get_api():
    api = NinjaAPI(
        title="backend",
        version="1.0.0",
        auth=[HTTPJWTAuth()],
    )

    add_exception_handlers(api)
    add_users_api(api)
    add_bank_accounts_api(api)
    add_cards_api(api)
    add_transactions_api(api)
    add_auth_api(api)

    return api
