from ninja import NinjaAPI

from app.internal.exceptions.exceptions import (
    BadRequestException,
    ForbiddenException,
    NotFoundException,
    UnauthorizedException,
)


def add_exception_handlers(api: NinjaAPI):
    @api.exception_handler(BadRequestException)
    def bad_request_exception_handler(request, exc):
        return api.create_response(
            request,
            {"message": f"{exc.message}"},
            status=400,
        )

    @api.exception_handler(UnauthorizedException)
    def unauthorized_exception_handler(request, exc):
        return api.create_response(
            request,
            {"message": "unauthorized"},
            status=401,
        )

    @api.exception_handler(ForbiddenException)
    def forbidden_exception_handler(request, exc):
        return api.create_response(
            request,
            {"message": f"{exc.message}"},
            status=403,
        )

    @api.exception_handler(NotFoundException)
    def object_not_found_exception_handler(request, exc):
        return api.create_response(
            request,
            {"message": f"{exc.name} with id {exc.id} not found"},
            status=404,
        )
