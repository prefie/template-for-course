class NotFoundException(Exception):
    def __init__(self, id, name):
        super().__init__()
        self.name = name
        self.id = id


class UnauthorizedException(Exception):
    pass


class BadRequestException(Exception):
    def __init__(self, message):
        super().__init__()
        self.message = message


class ForbiddenException(Exception):
    def __init__(self):
        super().__init__()
        self.message = "permission denied"
