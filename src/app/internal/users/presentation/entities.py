from uuid import UUID

from ninja.orm import create_schema

from ..db.models import User

UserSchema = create_schema(User, exclude=["telegram_id", "password"])


class UserOut(UserSchema):
    telegram_id: UUID
