from ninja import Body

from app.internal.exceptions.exceptions import NotFoundException
from app.internal.responses.responses import OkResponse
from app.internal.users.presentation.entities import UserOut


class UserHandlers:
    def __init__(self, user_repository):
        self._user_repo = user_repository

    def get_me(self, request):
        user = self._user_repo.get_user(request.user_id)
        return UserOut.from_orm(user)

    def set_phone(self, request, phone: str = Body(...)):
        if not self._user_repo.set_phone(request.user_id, phone):
            raise NotFoundException(request.user_id, "user")

    def delete_favorite_user(self, request, nickname):
        if self._user_repo.remove_user_from_favorites(request.user_id, nickname):
            return OkResponse()

        raise NotFoundException(nickname, "favorite_user")

    def add_user_in_favorites(self, request, nickname):
        if self._user_repo.add_user_to_favorites(request.user_id, nickname):
            return OkResponse()

        raise NotFoundException(nickname, "user")
