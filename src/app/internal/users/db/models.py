from django.db import models


class User(models.Model):
    telegram_id = models.IntegerField(primary_key=True)
    password = models.CharField(max_length=255, null=True)
    nickname = models.CharField(max_length=255, unique=True, null=True)
    first_name = models.CharField(max_length=255, null=True)
    last_name = models.CharField(max_length=255, null=True)
    phone_number = models.CharField(max_length=255, blank=True)
    featured_users = models.ManyToManyField("self", symmetrical=False, blank=True)

    objects = models.Manager()
