import hashlib

from django.conf import settings
from django.db import transaction
from django.db.models import Q
from phonenumbers import NumberParseException, is_valid_number, parse as parse_phone_number

from ...bot.constants import FORMAT_PHONE_ERROR
from ...transactions.db.models import Transaction
from ..presentation.entities import UserOut
from .models import User


class UserRepository:
    def create_if_not_exist(self, telegram_id, nickname, first_name, last_name) -> UserOut:
        obj, created = User.objects.get_or_create(
            telegram_id=telegram_id, nickname=nickname, first_name=first_name, last_name=last_name
        )

        return created

    def get_user_id_by_nickname(self, nickname):
        return User.objects.filter(nickname=nickname).values_list("telegram_id", flat=True).first()

    def set_phone(self, telegram_id, phone_number):
        user = self.get_user(telegram_id)
        if user:
            try:
                phone = parse_phone_number(phone_number)
                if not is_valid_number(phone):
                    raise NumberParseException(error_type=NumberParseException.NOT_A_NUMBER, msg=FORMAT_PHONE_ERROR)
                user.phone_number = phone_number
                user.save()
                return True
            except NumberParseException:
                return False
        return False

    def get_user(self, telegram_id):
        return User.objects.filter(telegram_id=telegram_id).first()

    def get_user_by_nickname(self, nickname):
        return User.objects.filter(nickname=nickname).first()

    def hash_password(self, password):
        return hashlib.sha1((settings.JWT_SECRET + password).encode()).hexdigest()

    def set_password(self, telegram_id, password):
        user = self.get_user(telegram_id)
        if user:
            user.password = self.hash_password(password)
            user.save()
            return True
        return False

    def get_featured_users(self, telegram_id):
        user = User.objects.prefetch_related("featured_users").filter(
            Q(telegram_id=telegram_id) & ~Q(featured_users=None)
        )
        if user.exists():
            return user.values_list("featured_users__nickname", flat=True)
        return None

    def check_user_in_favorites(self, telegram_id, user_telegram_id):
        user = User.objects.filter(telegram_id=telegram_id).first()
        if user:
            return user.featured_users.filter(telegram_id=user_telegram_id).values("telegram_id").exists()
        return False

    def add_user_to_favorites(self, telegram_id, nickname_to_add):
        user = User.objects.filter(telegram_id=telegram_id).first()
        user_to_add = User.objects.filter(nickname=nickname_to_add).first()
        if user and user_to_add:
            user.featured_users.add(user_to_add)
            user.save()
            return True
        return False

    def remove_user_from_favorites(self, telegram_id, nickname_to_remove):
        user = User.objects.filter(telegram_id=telegram_id).first()
        user_to_remove = User.objects.filter(nickname=nickname_to_remove).first()
        if user and user_to_remove:
            user.featured_users.remove(user_to_remove)
            user.save()
            return True
        return False

    def transfer_money(self, user_account, user_to_transfer_account, amount):
        if user_account.amount < amount:
            return False

        with transaction.atomic():
            user_account.amount -= amount
            user_to_transfer_account.amount += amount
            user_account.save()
            user_to_transfer_account.save()
            Transaction.objects.create(
                bank_account_from=user_account, bank_account_to=user_to_transfer_account, amount=amount
            )
            return True

    def get_interacted_users(self, bank_accounts):
        return Transaction.objects.filter(
            Q(bank_account_to__number__in=bank_accounts) | Q(bank_account_from__number__in=bank_accounts)
        ).values("bank_account_from__owner__nickname", "bank_account_to__owner__nickname")
