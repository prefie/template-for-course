from ninja import Schema


class OkResponse(Schema):
    pass


class ErrorResponse(Schema):
    message: str
