def serialize_user(user):
    return dict(
        nickname=user.nickname,
        first_name=user.first_name,
        last_name=user.last_name,
        telegram_id=user.telegram_id,
        phone_number=user.phone_number,
    )
