from phonenumbers import NumberParseException, is_valid_number, parse as parse_phone_number

from app.internal.bot.constants import (
    ADD_TO_FAVORITES_LIST_MESSAGE,
    BANK_ACCOUNT_ERROR,
    CARD_ERROR,
    EMPTY_FAVORITES_LIST_MESSAGE,
    ERROR,
    FORMAT_PHONE_ERROR,
    INVALID_PHONE_ERROR,
    NO_INTERACTED_USERS_ERROR,
    NO_TRANSACTIONS_ERROR,
    NOT_FOUND_BANK_ACCOUNT_ERROR,
    NOT_FOUND_CARD_ERROR,
    NOT_POSITIVE_NUMBER_ERROR,
    REMOVE_FROM_FAVORITES_LIST_MESSAGE,
    SAVE_PHONE_MESSAGE,
    SET_PASSWORD_ERROR,
    SET_PASSWORD_MESSAGE,
    START_MESSAGE,
    TRANSFER_MONEY_MESSAGE,
    TRANSFER_TO_YOURSELF_ERROR,
    USER_EXIST_ERROR,
    USER_NOT_FOUND_ERROR,
    USER_NOT_IN_FAVORITES_ERROR,
)
from app.internal.bot.validators import is_existing_user


class BotService:
    def __init__(self, bank_account_repo, card_repo, user_repo):
        self._bank_account_repo = bank_account_repo
        self._card_repo = card_repo
        self._user_repo = user_repo

    def handle_start(self, telegram_id, nickname, first_name, last_name):
        if self._user_repo.create_if_not_exist(telegram_id, nickname, first_name, last_name):
            return START_MESSAGE

        return USER_EXIST_ERROR

    @is_existing_user
    def handle_set_phone(self, telegram_id, phone_number):
        try:
            phone = parse_phone_number(phone_number)
            if not is_valid_number(phone):
                raise NumberParseException(error_type=NumberParseException.NOT_A_NUMBER, msg=FORMAT_PHONE_ERROR)

            self._user_repo.set_phone(telegram_id, phone_number)
            return SAVE_PHONE_MESSAGE

        except NumberParseException:
            return INVALID_PHONE_ERROR

    @is_existing_user
    def handle_set_password(self, telegram_id, password):
        if self._user_repo.set_password(telegram_id, password):
            return SET_PASSWORD_MESSAGE

        return SET_PASSWORD_ERROR

    @is_existing_user
    def handle_get_user(self, telegram_id):
        user = self._user_repo.get_user(telegram_id)
        message = (
            f"Вот что я о тебе знаю:\n"
            f"Твой ник @{user.nickname}, "
            f"а зовут тебя {user.first_name} {'' if user.last_name is None else user.last_name}"
        )
        if user.phone_number:
            message += f"\nИ ещё я знаю твой номер телефона: {user.phone_number}"
        return message

    @is_existing_user
    def handle_get_bank_accounts(self, telegram_id):
        bank_accounts = self._bank_account_repo.get_bank_account_numbers(telegram_id)
        if len(bank_accounts) < 1:
            return NOT_FOUND_BANK_ACCOUNT_ERROR

        answer = "\n".join(bank_accounts)
        return f"Вот все твои счета:\n{answer}"

    @is_existing_user
    def handle_get_amount_by_bank_account(self, telegram_id, bank_account_number):
        bank_account = self._bank_account_repo.get_bank_account(bank_account_number)
        if not bank_account or bank_account.owner.telegram_id != telegram_id:
            return BANK_ACCOUNT_ERROR

        return f"На счету: {bank_account.amount}"

    @is_existing_user
    def handle_get_cards(self, telegram_id):
        cards = self._card_repo.get_cards(telegram_id)
        if len(cards) < 1:
            return NOT_FOUND_CARD_ERROR

        answer = "\n".join(cards)
        return f"Вот все твои карты:\n{answer}"

    @is_existing_user
    def handle_get_amount_by_card(self, telegram_id, card_number):
        card = self._card_repo.get_card(card_number)
        if card is None or card.bank_account.owner.telegram_id != telegram_id:
            return CARD_ERROR

        return f"Столько деняк на карте: {card.bank_account.amount}"

    @is_existing_user
    def handle_get_featured_users(self, telegram_id):
        featured_users = self._user_repo.get_featured_users(telegram_id)
        if not featured_users or len(featured_users) < 1:
            return EMPTY_FAVORITES_LIST_MESSAGE

        return ",\n".join(featured_users)

    @is_existing_user
    def handle_add_user_to_favorites(self, telegram_id, nickname):
        if self._user_repo.add_user_to_favorites(telegram_id, nickname):
            return ADD_TO_FAVORITES_LIST_MESSAGE

        return USER_NOT_FOUND_ERROR

    @is_existing_user
    def handle_remove_user_from_favorites(self, telegram_id, nickname):
        if self._user_repo.remove_user_from_favorites(telegram_id, nickname):
            return REMOVE_FROM_FAVORITES_LIST_MESSAGE

        return USER_NOT_FOUND_ERROR

    @is_existing_user
    def handle_transfer_money(self, telegram_id, nickname, amount):
        tg_id = self._user_repo.get_user_id_by_nickname(nickname)
        if not self._user_repo.check_user_in_favorites(tg_id):
            return USER_NOT_IN_FAVORITES_ERROR
        if amount <= 0:
            return NOT_POSITIVE_NUMBER_ERROR
        else:
            user = self._user_repo.get_user(telegram_id)
            user_to_transfer = self._user_repo.get_user_by_nickname(nickname)
            if user and user_to_transfer:
                user_bank_accounts = self._bank_account_repo.get_bank_account_numbers(str(user.telegram_id))
                user_to_transfer_bank_accounts = self._bank_account_repo.get_bank_account_numbers(
                    str(user_to_transfer.telegram_id)
                )
                user_account = self._bank_account_repo.get_bank_account(user_bank_accounts[0])
                user_to_transfer_account = self._bank_account_repo.get_bank_account(user_to_transfer_bank_accounts[0])
                if not self._user_repo.transfer_money(user_account, user_to_transfer_account, amount):
                    return ERROR

        return TRANSFER_MONEY_MESSAGE

    @is_existing_user
    def handle_transfer_money_by_bank_accounts(
        self, telegram_id, bank_account_number, bank_account_to_transfer_number, amount
    ):
        bank_account = self._bank_account_repo.get_bank_account(bank_account_number)
        if not bank_account or bank_account.owner.telegram_id != telegram_id:
            return BANK_ACCOUNT_ERROR
        if bank_account_number == bank_account_to_transfer_number:
            return TRANSFER_TO_YOURSELF_ERROR
        if amount <= 0:
            return NOT_POSITIVE_NUMBER_ERROR
        elif not self._bank_account_repo.transfer_money_by_bank_account(
            bank_account_number, bank_account_to_transfer_number, amount
        ):
            return ERROR

        return TRANSFER_MONEY_MESSAGE

    @is_existing_user
    def handle_transfer_money_by_cards(self, telegram_id, card_number, card_to_transfer_number, amount):
        card = self._card_repo.get_card(card_number)
        if not card or card.bank_account.owner.telegram_id != telegram_id:
            return CARD_ERROR
        card_to_transfer = self._card_repo.get_card(card_to_transfer_number)
        if not card_to_transfer:
            return ERROR
        if card.bank_account.number == card_to_transfer.bank_account.number:
            return TRANSFER_TO_YOURSELF_ERROR
        if amount <= 0:
            return NOT_POSITIVE_NUMBER_ERROR
        elif not self._card_repo.transfer_money_by_card(card_number, card_to_transfer_number, amount):
            return ERROR

        return TRANSFER_MONEY_MESSAGE

    @is_existing_user
    def handle_get_bank_account_statement(self, telegram_id, bank_account_number):
        bank_account = self._bank_account_repo.get_bank_account(bank_account_number)
        if not bank_account or bank_account.owner.telegram_id != telegram_id:
            return BANK_ACCOUNT_ERROR
        transactions = self._bank_account_repo.get_bank_account_transactions(bank_account_number)
        if len(transactions) < 1:
            return NO_TRANSACTIONS_ERROR

        answer = []
        for transaction in transactions:
            answer.append(f"ПЕРЕВОД\nОт кого: @{transaction['bank_account_from__owner__nickname']}")
            answer.append(f"Со счета: {transaction['bank_account_from__number']}")
            answer.append(f"Кому: @{transaction['bank_account_to__owner__nickname']}")
            answer.append(f"На счет: {transaction['bank_account_to__number']}")
            answer.append(f"Сумма: {transaction['amount']}")
            answer.append(f"Дата операции: {transaction['date']}")
            answer.append("____________________")
        return "\n".join(answer)

    @is_existing_user
    def handle_get_card_statement(self, telegram_id, card_number):
        card = self._card_repo.get_card(card_number)
        if not card or card.bank_account.owner.telegram_id != telegram_id:
            return CARD_ERROR
        transactions = self._card_repo.get_card_transactions(card_number)
        if len(transactions) < 1:
            return NO_TRANSACTIONS_ERROR

        answer = []
        for transaction in transactions:
            answer.append(f"ПЕРЕВОД\nОт кого: @{transaction['card_from__bank_account__owner__nickname']}")
            answer.append(f"С карты: {transaction['card_from__number']}")
            answer.append(f"Кому: @{transaction['card_to__bank_account__owner__nickname']}")
            answer.append(f"На карту: {transaction['card_to__number']}")
            answer.append(f"Сумма: {transaction['amount']}")
            answer.append(f"Дата операции: {transaction['date']}")
            answer.append("____________________")
        return "\n".join(answer)

    @is_existing_user
    def handle_get_interacted_users(self, telegram_id, nickname):
        bank_accounts = self._bank_account_repo.get_bank_account_numbers(telegram_id)
        inter_users = self._user_repo.get_interacted_users(bank_accounts)
        interacted_users = BotService._filter_interacted_users(inter_users, nickname)
        if len(interacted_users) < 1:
            return NO_INTERACTED_USERS_ERROR
        answer = ["Вы взаимодействовали с:"]
        for user in interacted_users:
            answer.append(f"@{user}")
        return "\n".join(answer)

    @staticmethod
    def _filter_interacted_users(interacted_users, nickname):
        answer = set()
        for user in interacted_users:
            if user["bank_account_to__owner__nickname"] != nickname:
                answer.add(user["bank_account_to__owner__nickname"])
            else:
                answer.add(user["bank_account_from__owner__nickname"])

        return answer
