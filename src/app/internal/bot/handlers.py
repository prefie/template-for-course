from app.internal.bank_accounts.db.repositories import BankAccountRepository
from app.internal.bot.bot_service import BotService
from app.internal.bot.constants import (
    ADD_TO_FAVORITES_ERROR,
    BANK_ACCOUNT_STATEMENT_PARAMS_ERROR,
    CARD_STATEMENT_PARAMS_ERROR,
    FORMAT_CARD_ERROR,
    FORMAT_PHONE_ERROR,
    REMOVE_FROM_FAVORITES_ERROR,
    SET_PASSWORD_PARAMS_ERROR,
    TRANSFER_MONEY_BY_ACCOUNT_PARAMS_ERROR,
    TRANSFER_MONEY_BY_CARD_PARAMS_ERROR,
    TRANSFER_MONEY_PARAMS_ERROR,
)
from app.internal.cards.db.repositories import CardRepository
from app.internal.users.db.repositories import UserRepository

bot_service = BotService(BankAccountRepository(), CardRepository(), UserRepository())


def start(update, context):
    user = update.effective_user
    response = bot_service.handle_start(user.id, user.username, user.first_name, user.last_name)
    update.message.reply_text(response)


def set_phone(update, context):
    if len(context.args) != 1:
        update.message.reply_text(FORMAT_PHONE_ERROR)
        return

    response = bot_service.handle_set_phone(update.effective_user.id, context.args[0])
    update.message.reply_text(response)


def set_password(update, context):
    if len(context.args) != 1:
        update.message.reply_text(SET_PASSWORD_PARAMS_ERROR)
        return

    response = bot_service.handle_set_password(update.effective_user.id, context.args[0])
    update.message.reply_text(response)


def get_user(update, context):
    response = bot_service.handle_get_user(update.effective_user.id)
    update.message.reply_text(response)


def get_bank_accounts(update, context):
    response = bot_service.handle_get_bank_accounts(update.effective_user.id)
    update.message.reply_text(response)


def get_amount_by_bank_account(update, context):
    if len(context.args) != 1:
        update.message.reply_text(FORMAT_CARD_ERROR)
        return
    response = bot_service.handle_get_amount_by_bank_account(update.effective_user.id, context.args[0])
    update.message.reply_text(response)


def get_cards(update, context):
    response = bot_service.handle_get_cards(update.effective_user.id)
    update.message.reply_text(response)


def get_amount_by_card(update, context):
    if len(context.args) != 1:
        update.message.reply_text(FORMAT_CARD_ERROR)
        return

    response = bot_service.handle_get_amount_by_card(update.effective_user.id, context.args[0])
    update.message.reply_text(response)


def get_featured_users(update, context):
    response = bot_service.handle_get_featured_users(update.effective_user.id)
    update.message.reply_text(response)


def add_user_to_favorites(update, context):
    if len(context.args) != 1:
        update.message.reply_text(ADD_TO_FAVORITES_ERROR)
        return

    response = bot_service.handle_add_user_to_favorites(update.effective_user.id, context.args[0][1:])
    update.message.reply_text(response)


def remove_user_from_favorites(update, context):
    if len(context.args) != 1:
        update.message.reply_text(REMOVE_FROM_FAVORITES_ERROR)
        return

    response = bot_service.handle_remove_user_from_favorites(update.effective_user.id, context.args[0][1:])
    update.message.reply_text(response)


def transfer_money(update, context):
    if len(context.args) != 2 or not context.args[1].isdigit():
        update.message.reply_text(TRANSFER_MONEY_PARAMS_ERROR)
        return

    response = bot_service.handle_transfer_money(update.effective_user.id, context.args[0][1:], int(context.args[1]))
    update.message.reply_text(response)


def transfer_money_by_bank_accounts(update, context):
    if len(context.args) != 3 or not context.args[2].isdigit():
        update.message.reply_text(TRANSFER_MONEY_BY_ACCOUNT_PARAMS_ERROR)
        return

    response = bot_service.handle_transfer_money_by_bank_accounts(
        update.effective_user.id, context.args[0], context.args[1], int(context.args[2])
    )
    update.message.reply_text(response)


def transfer_money_by_cards(update, context):
    if len(context.args) != 3 or not context.args[2].isdigit():
        update.message.reply_text(TRANSFER_MONEY_BY_CARD_PARAMS_ERROR)
        return

    response = bot_service.handle_transfer_money_by_cards(
        update.effective_user.id, context.args[0], context.args[1], int(context.args[2])
    )
    update.message.reply_text(response)


def get_bank_account_statement(update, context):
    if len(context.args) != 1:
        update.message.reply_text(BANK_ACCOUNT_STATEMENT_PARAMS_ERROR)
        return

    response = bot_service.handle_get_bank_account_statement(update.effective_user.id, context.args[0])
    update.message.reply_text(response)


def get_card_statement(update, context):
    if len(context.args) != 1:
        update.message.reply_text(CARD_STATEMENT_PARAMS_ERROR)
        return

    response = bot_service.handle_get_card_statement(update.effective_user.id, context.args[0])
    update.message.reply_text(response)


def get_interacted_users(update, context):
    response = bot_service.handle_get_interacted_users(update.effective_user.id, update.effective_user.username)
    update.message.reply_text(response)
