import json

from django.http import JsonResponse
from django.views import View

from app.internal.bot.bot import TelegramBot
from config.settings import TELEGRAM_TOKEN

bot = TelegramBot(TELEGRAM_TOKEN)


class BotView(View):
    def post(self, request, *args, **kwargs):
        t_data = json.loads(request.body)
        bot.process(t_data)
        return JsonResponse({"OK": "POST request processed"})
