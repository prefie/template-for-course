from telegram import Bot, Update
from telegram.ext import CommandHandler, Dispatcher

from app.internal.bot.handlers import (
    add_user_to_favorites,
    get_amount_by_bank_account,
    get_amount_by_card,
    get_bank_account_statement,
    get_bank_accounts,
    get_card_statement,
    get_cards,
    get_featured_users,
    get_interacted_users,
    get_user,
    remove_user_from_favorites,
    set_password,
    set_phone,
    start,
    transfer_money,
    transfer_money_by_bank_accounts,
    transfer_money_by_cards,
)


class TelegramBot:
    def __init__(self, token):
        self.bot = Bot(token)
        self.dispatcher = Dispatcher(self.bot, None, workers=1)

        self.start()

    def start(self):
        dp = self.dispatcher
        dp.add_handler(CommandHandler("start", start))
        dp.add_handler(CommandHandler("set_phone", set_phone))
        dp.add_handler(CommandHandler("me", get_user))
        dp.add_handler(CommandHandler("get_bank_accounts", get_bank_accounts))
        dp.add_handler(CommandHandler("get_amount_by_bank_account", get_amount_by_bank_account))
        dp.add_handler(CommandHandler("get_cards", get_cards))
        dp.add_handler(CommandHandler("get_amount_by_card", get_amount_by_card))
        dp.add_handler(CommandHandler("get_featured_users", get_featured_users))
        dp.add_handler(CommandHandler("add_user_to_favorites", add_user_to_favorites))
        dp.add_handler(CommandHandler("remove_user_from_favorites", remove_user_from_favorites))
        dp.add_handler(CommandHandler("transfer_money", transfer_money))
        dp.add_handler(CommandHandler("transfer_money_by_cards", transfer_money_by_cards))
        dp.add_handler(CommandHandler("transfer_money_by_bank_accounts", transfer_money_by_bank_accounts))
        dp.add_handler(CommandHandler("bank_account_statement", get_bank_account_statement))
        dp.add_handler(CommandHandler("card_statement", get_card_statement))
        dp.add_handler(CommandHandler("get_interacted_users", get_interacted_users))
        dp.add_handler(CommandHandler("set_password", set_password))

    def process(self, t_data):
        update = Update.de_json(t_data, self.bot)
        self.dispatcher.process_update(update)
