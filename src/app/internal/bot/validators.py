import functools
from uuid import UUID

from app.internal.bot.constants import START_ERROR
from app.internal.users.db.models import User


def validate_uuid4(uuid_string):
    try:
        value = UUID(uuid_string, version=4)
    except ValueError:
        return False

    return str(value) == uuid_string


def is_existing_user(func):
    @functools.wraps(func)
    def wrapped(self, telegram_id, *args, **kwargs):
        if User.objects.filter(telegram_id=telegram_id).exists():
            return func(self, telegram_id, *args, **kwargs)
        return START_ERROR

    return wrapped
