import json

import pytest

from app.internal.serializers import serialize_user


@pytest.mark.unit
@pytest.mark.django_db
@pytest.mark.parametrize("phone_number", ["", "+7-999-999-99-99"])
def test_serialize_user(test_user, phone_number, user_repo):
    user_repo.set_phone(test_user.telegram_id, phone_number)

    assert serialize_user(test_user) == {
        "nickname": test_user.nickname,
        "first_name": test_user.first_name,
        "last_name": test_user.last_name,
        "telegram_id": test_user.telegram_id,
        "phone_number": test_user.phone_number,
    }


@pytest.mark.unit
@pytest.mark.django_db
def test_me_when_new_user(test_user, test_django_client):
    '''response = test_django_client.post(f"/api/auth/login", {
        "nickname": '123',
        "password": '123',
    })
    response_json = json.loads(response.content)
    headers = {
        "HTTP_AUTHORIZATION": "Bearer " + response_json["access_token"],
    }

    res = test_django_client.get("/api/me", **headers)
    assert res.status_code == 200
    res_json = json.loads(res.content)
    assert res_json["nickname"] == test_user.nickname
    assert res_json["first_name"] == test_user.first_name
    assert res_json["last_name"] == test_user.last_name
    assert res_json["phone_number"] == ""'''


@pytest.mark.unit
@pytest.mark.django_db
def test_me_with_set_phone(test_user, test_django_client, user_repo):
    """user_repo.set_phone(test_user.telegram_id, "+7-999-999-99-99")
    response = test_django_client.post(f"/api/auth/login", {
        "login": {
            "nickname": '123',
            "password": '123',
        }
    })
    response_json = json.loads(response.content)
    headers = {
        "HTTP_AUTHORIZATION": "Bearer " + response_json["access_token"],
    }

    res = test_django_client.get("/api/me", **headers)
    assert res.status_code == 200
    res_json = json.loads(res.content)
    assert res_json["nickname"] == test_user.nickname
    assert res_json["telegram_id"] == test_user.telegram_id
    assert res_json["first_name"] == test_user.first_name
    assert res_json["last_name"] == test_user.last_name"""


@pytest.mark.unit
@pytest.mark.django_db
def test_me_when_no_user(test_django_client):
    res = test_django_client.get("/api/auth/me")
    assert res.status_code == 404


@pytest.mark.unit
@pytest.mark.django_db
def test_add_user_to_favorites(test_user, test_other_user, user_repo):
    user_repo.add_user_to_favorites(test_user.telegram_id, test_other_user.nickname)
    assert test_other_user in test_user.featured_users.all()


@pytest.mark.unit
@pytest.mark.django_db
def test_remove_user_from_favorites(test_user, test_other_user, user_repo):
    user_repo.add_user_to_favorites(test_user.telegram_id, test_other_user.nickname)
    user_repo.remove_user_from_favorites(test_user.telegram_id, test_other_user.nickname)
    assert test_other_user not in test_user.featured_users.all()


@pytest.mark.unit
@pytest.mark.django_db
def test_get_featured_users(test_user_with_featured_users, test_other_user, user_repo):
    users = user_repo.get_featured_users(test_user_with_featured_users.telegram_id)

    assert test_other_user.nickname in users
