import pytest

from app.internal.bank_accounts.db.models import BankAccount


@pytest.mark.unit
@pytest.mark.parametrize(("amount", "successful"), [(1, True), (0, False), (-1, False), (9999, False)])
@pytest.mark.django_db
def test_transfer_money_by_bank_account(
    test_bank_account, test_other_bank_account, bank_account_repo, amount, successful
):
    assert (
        bank_account_repo.transfer_money_by_bank_account(
            test_bank_account.number, test_other_bank_account.number, amount
        )
        == successful
    )

    bank_account = BankAccount.objects.get(number=test_bank_account.number)
    bank_account_to_transfer = BankAccount.objects.get(number=test_other_bank_account.number)
    if successful:
        assert bank_account.amount == test_bank_account.amount - amount
        assert bank_account_to_transfer.amount == test_other_bank_account.amount + amount
