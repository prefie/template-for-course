import pytest

from app.internal.bank_accounts.db.models import BankAccount


@pytest.mark.unit
@pytest.mark.parametrize(("amount", "should_successfully"), [(1, True), (0, False), (-1, False), (9999, False)])
@pytest.mark.django_db
def test_transfer_money_by_card(test_card, test_other_card, card_repo, amount, should_successfully):
    assert card_repo.transfer_money_by_card(test_card.number, test_other_card.number, amount) == should_successfully

    bank_account = BankAccount.objects.get(number=test_card.bank_account.number)
    bank_account_to_transfer = BankAccount.objects.get(number=test_other_card.bank_account.number)
    if should_successfully:
        assert bank_account.amount == test_card.bank_account.amount - amount
        assert bank_account_to_transfer.amount == test_other_card.bank_account.amount + amount
