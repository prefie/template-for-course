import pytest
from django.test import Client

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.bank_accounts.db.repositories import BankAccountRepository
from app.internal.cards.db.models import Card
from app.internal.cards.db.repositories import CardRepository
from app.internal.users.db.models import User
from app.internal.users.db.repositories import UserRepository


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_user(user_repo):
    return User.objects.create(
        telegram_id=0,
        nickname="nickname",
        first_name="first",
        last_name="last",
        password=user_repo.hash_password("123"),
    )


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_other_user():
    return User.objects.create(telegram_id=1, nickname="nickname1")


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_user_with_featured_users(test_user, test_other_user, user_repo):
    user_repo.add_user_to_favorites(test_user.telegram_id, test_other_user.nickname)
    return User.objects.get(telegram_id=test_user.telegram_id)


@pytest.fixture()
def test_django_client():
    return Client()


@pytest.fixture()
@pytest.mark.django_db
def user_repo():
    return UserRepository()


@pytest.fixture()
@pytest.mark.django_db
def card_repo():
    return CardRepository()


@pytest.fixture()
@pytest.mark.django_db
def bank_account_repo():
    return BankAccountRepository()


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_bank_account(test_user):
    return BankAccount.objects.create(owner=test_user, amount=100)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_other_bank_account(test_other_user):
    return BankAccount.objects.create(owner=test_other_user, amount=100)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_card(test_bank_account):
    return Card.objects.create(bank_account=test_bank_account)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_other_card(test_other_bank_account):
    return Card.objects.create(bank_account=test_other_bank_account)
