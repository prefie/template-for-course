import pytest

from app.internal.bot.bot_service import BotService


@pytest.fixture(scope="function")
def test_telegram_user():
    return {"id": 0, "username": "username", "first_name": "first", "last_name": "last"}


@pytest.fixture()
@pytest.mark.django_db
def bot_service(bank_account_repo, card_repo, user_repo):
    return BotService(bank_account_repo, card_repo, user_repo)
