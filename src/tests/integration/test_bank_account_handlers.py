import pytest

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.bot.constants import BANK_ACCOUNT_ERROR, ERROR, NOT_POSITIVE_NUMBER_ERROR, TRANSFER_MONEY_MESSAGE


@pytest.mark.integration
@pytest.mark.django_db
@pytest.mark.parametrize(
    ("amount", "answer"),
    [(1, TRANSFER_MONEY_MESSAGE), (0, NOT_POSITIVE_NUMBER_ERROR), (-1, NOT_POSITIVE_NUMBER_ERROR), (9999, ERROR)],
)
def test_transfer_money_by_bank_accounts(
    test_user, test_bank_account, test_other_bank_account, bot_service, amount, answer
):
    response = bot_service.handle_transfer_money_by_bank_accounts(
        test_user.telegram_id, test_bank_account.number, test_other_bank_account.number, amount
    )
    assert response == answer
    if response == TRANSFER_MONEY_MESSAGE:
        bank_account = BankAccount.objects.get(number=test_bank_account.number)
        bank_account_to_transfer = BankAccount.objects.get(number=test_other_bank_account.number)
        assert bank_account.amount == test_bank_account.amount - amount
        assert bank_account_to_transfer.amount == test_other_bank_account.amount + amount


@pytest.mark.integration
@pytest.mark.django_db
def test_transfer_money_by_bank_accounts_with_non_existent_account(test_user, test_bank_account, bot_service):
    response = bot_service.handle_transfer_money_by_bank_accounts(
        test_user.telegram_id, test_bank_account.number, "99999999999999999999", 1
    )

    assert response == ERROR
    response = bot_service.handle_transfer_money_by_bank_accounts(
        test_user.telegram_id, "99999999999999999999", test_bank_account.number, 1
    )
    assert response == BANK_ACCOUNT_ERROR


@pytest.mark.integration
@pytest.mark.django_db
def test_get_bank_account_statement(
    test_user, test_other_user, test_bank_account, test_other_bank_account, bot_service
):
    bot_service.handle_get_bank_accounts(test_user.telegram_id)
    amount = 10
    bot_service.handle_transfer_money_by_bank_accounts(
        test_user.telegram_id, test_bank_account.number, test_other_bank_account.number, amount
    )

    response = bot_service.handle_get_bank_account_statement(test_user.telegram_id, test_bank_account.number)
    assert test_user.nickname in response
    assert test_other_user.nickname in response
    assert test_bank_account.number in response
    assert test_other_bank_account.number in response
    assert str(amount) in response
