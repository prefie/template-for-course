import pytest

from app.internal.bot.constants import (
    ADD_TO_FAVORITES_LIST_MESSAGE,
    EMPTY_FAVORITES_LIST_MESSAGE,
    INVALID_PHONE_ERROR,
    REMOVE_FROM_FAVORITES_LIST_MESSAGE,
    SAVE_PHONE_MESSAGE,
    START_ERROR,
    START_MESSAGE,
    USER_EXIST_ERROR,
)
from app.internal.users.db.models import User


@pytest.mark.integration
@pytest.mark.django_db
def test_start_handler_with_new_user(test_telegram_user, bot_service):
    response = bot_service.handle_start(
        test_telegram_user["id"],
        test_telegram_user["username"],
        test_telegram_user["first_name"],
        test_telegram_user["last_name"],
    )

    assert User.objects.get(telegram_id=test_telegram_user["id"])
    assert response == START_MESSAGE


@pytest.mark.integration
@pytest.mark.django_db
def test_start_handler_with_existing_user(test_user, bot_service):
    response = bot_service.handle_start(
        test_user.telegram_id,
        test_user.nickname,
        test_user.first_name,
        test_user.last_name,
    )

    assert User.objects.get(telegram_id=test_user.telegram_id)
    assert response == USER_EXIST_ERROR


@pytest.mark.integration
@pytest.mark.django_db
@pytest.mark.parametrize(
    ("phone_number", "answer"),
    [
        ("qwe", INVALID_PHONE_ERROR),
        ("+7-999-999-9", INVALID_PHONE_ERROR),
        ("+79999999999", SAVE_PHONE_MESSAGE),
        ("+7-999-999-99-99", SAVE_PHONE_MESSAGE),
    ],
)
def test_set_phone(test_user, bot_service, phone_number, answer):
    response = bot_service.handle_set_phone(test_user.telegram_id, phone_number)
    assert response == answer


@pytest.mark.integration
@pytest.mark.django_db
def test_non_existent_user(bot_service):
    response = bot_service.handle_get_user(100)
    assert response == START_ERROR


@pytest.mark.integration
@pytest.mark.django_db
def test_get_user(test_user, user_repo, bot_service):
    phone_number = "+7-999-999-99-99"
    user_repo.set_phone(test_user.telegram_id, phone_number)
    response = bot_service.handle_get_user(test_user.telegram_id)
    assert test_user.nickname in response
    assert test_user.last_name in response
    assert phone_number in response


@pytest.mark.integration
@pytest.mark.django_db
def test_get_featured_users(test_user_with_featured_users, test_other_user, bot_service):
    response = bot_service.handle_get_featured_users(test_user_with_featured_users.telegram_id)

    assert test_other_user.nickname in response


@pytest.mark.integration
@pytest.mark.django_db
def test_get_featured_users_when_empty(test_user, bot_service):
    response = bot_service.handle_get_featured_users(test_user.telegram_id)

    assert response == EMPTY_FAVORITES_LIST_MESSAGE


@pytest.mark.integration
@pytest.mark.django_db
def test_add_user_to_favorites(test_user, test_other_user, user_repo, bot_service):
    response = bot_service.handle_add_user_to_favorites(test_user.telegram_id, test_other_user.nickname)

    assert response == ADD_TO_FAVORITES_LIST_MESSAGE
    users = user_repo.get_featured_users(test_user.telegram_id)
    assert test_other_user.nickname in users


@pytest.mark.integration
@pytest.mark.django_db
def test_remove_user_from_favorites(test_user_with_featured_users, test_other_user, user_repo, bot_service):
    response = bot_service.handle_remove_user_from_favorites(
        test_user_with_featured_users.telegram_id, test_other_user.nickname
    )

    assert response == REMOVE_FROM_FAVORITES_LIST_MESSAGE
    users = user_repo.get_featured_users(test_user_with_featured_users.telegram_id)
    assert users is None or test_other_user not in users


@pytest.mark.integration
@pytest.mark.django_db
def test_interacted_users(test_user, test_other_user, test_card, test_other_card, bot_service):
    amount = 10
    bot_service.handle_transfer_money_by_cards(test_user.telegram_id, test_card.number, test_other_card.number, amount)

    response = bot_service.handle_get_interacted_users(test_user.telegram_id, test_user.nickname)
    assert test_other_user.nickname in response
